"""Configures and runs the vox discord bot
"""
import argparse
import asyncio
import logging
import os

from hlvoxdiscordbot.hl_vox_bot_client import HlVoxDiscordBot

logging.basicConfig(level=logging.DEBUG)


async def main():
    """Configures and runs the vox discord bot
    """
    if 'API_URL' in os.environ and 'TOKEN_PATH' in os.environ:
        api_url = os.environ.get('API_URL')
        token_path = os.environ.get('TOKEN_PATH')
    else:
        parser = argparse.ArgumentParser(description="Discord bot for hlvox")
        parser.add_argument('api_url', type=str, help="url for hlvox api")
        parser.add_argument('token_path', type=str, help="path to token file")

        args = parser.parse_args()

        api_url = args.api_url
        token_path = args.token_path

    with open(token_path, encoding='UTF-8') as token_file:
        token = token_file.readline()

    bot = HlVoxDiscordBot(
        api_url=api_url,
        play_lock=asyncio.Lock(),
        command_prefix='v!',
    )

    async with bot:
        await bot.start(token)

if __name__ == '__main__':
    asyncio.run(main())
