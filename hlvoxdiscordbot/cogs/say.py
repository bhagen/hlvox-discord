"""Say cog
Adds the ability to connect to and say sentences in voice channels.
"""
import json
import logging
import typing
from dataclasses import dataclass
from enum import Enum, auto

import discord
import requests
from discord import app_commands
from discord.ext import commands

REQUEST_TIMEOUT_S = 15
MAX_OPTIONS = 25

log = logging.getLogger()


@dataclass
class SayCommand:
    """Represents a command to say a sentence
    """
    sentence: str
    voice: str
    guild: discord.Guild
    channel: discord.VoiceChannel
    edit: typing.Callable


class CommandEnum(Enum):
    """Command types for say player queues
    """
    SKIP = auto()
    DISCONNECT = auto()


@dataclass
class Command:
    """Represents a command to an active player
    """
    ctx: typing.Union[commands.Context, discord.Interaction]
    command: CommandEnum

    def respond(self, text: str):
        if isinstance(self.ctx, commands.Context):
            return self.ctx.send(text)
        if isinstance(self.ctx, discord.Interaction):
            return self.ctx.response.send_message(text)
        else:
            raise Exception(f"Unimplemented context type: {type(self.ctx)}")


class Say(commands.Cog):
    """Say cog
    Adds the ability to connect to and say sentences in voice channels.
    """

    def __init__(self, bot: commands.Bot) -> None:
        """Initialize cog

        Args:
            bot (commands.Bot): Bot to attach cog to
        """
        self.bot = bot

    def verify_sentence(self, voice: str, sentence: str) -> typing.Optional[str]:
        """Verify voice is correct and sentence is sayable

        Args:
            voice (str): Voice to verify
            sentence (str): Sentence to verify

        Returns:
            typing.Optional[str]: Error string, or None if sentence is sayable
        """
        result = requests.get(
            f"{self.bot.api_url}/api/voices", timeout=REQUEST_TIMEOUT_S)
        result.raise_for_status()

        r_content = json.loads(result.content)

        if voice not in r_content['voices']:
            return f"Unknown voice {voice}"

        result = requests.get(
            f"{self.bot.api_url}/api/voice/{voice}/sentence_info", params={"sentence": sentence}, timeout=REQUEST_TIMEOUT_S)
        result.raise_for_status()

        r_content = json.loads(result.content)

        if len(r_content['sayable']) == 0:
            return "Couldn't say any of that"

        if len(r_content['unsayable']):
            return f"Couldn't say {r_content['unsayable']}"
        return None

    async def voice_autocomplete(
        self,
        _: discord.Interaction,
        current: str
    ) -> list[app_commands.Choice[str]]:
        """Autocomplete voice selection for slash commands

        Args:
            _ (discord.Interaction): Not used
            current (str): Current input string

        Returns:
            list[app_commands.Choice[str]]: Voices matching current input string
        """
        # TODO: cache this stuff so we don't spam the API
        result = requests.get(
            f"{self.bot.api_url}/api/voices", timeout=REQUEST_TIMEOUT_S)
        result.raise_for_status()

        voices = json.loads(result.content)['voices']
        options = []
        for voice in voices:
            if current.lower() in voice.lower():
                options.append(app_commands.Choice(name=voice, value=voice))
        return options

    async def sentence_autocomplete(
        self,
        interaction: discord.Interaction,
        current: str
    ) -> list[app_commands.Choice[str]]:
        """Autocomplete word/sentence selection for slash commands

        Args:
            interaction (discord.Interaction): Slash command interaction
            current (str): Current input string

        Raises:
            Exception: Something weird happened

        Returns:
            list[app_commands.Choice[str]]: Words matching current input string
        """
        # TODO: stealing the voice from this dict doesn't feel great, there may be a better way
        # TODO: there is a lot of optimization that could be done in this function
        voice = None
        data = interaction.data
        if data is None:
            raise Exception("No interaction data available")

        # mypy doesn't like this, and I can't seem to untangle the type hints it is using
        for option in data['options']:  # type: ignore
            if option['name'] == 'voice':  # type: ignore
                voice = option['value']  # type: ignore
                break

        current_words = current.split(' ')
        incomplete_word = current_words[-1]
        completed_sentence = ' '.join(current_words[:-1])

        # TODO: cache this stuff so we don't spam the API
        result = requests.get(
            f"{self.bot.api_url}/api/voice/{voice}", timeout=REQUEST_TIMEOUT_S)
        result.raise_for_status()

        words = json.loads(result.content)['words']
        words_filtered = [word for word in words if incomplete_word in word]
        options = []
        for word in words_filtered[:MAX_OPTIONS]:
            option = completed_sentence + ' ' + word
            options.append(app_commands.Choice(name=option, value=option))

        return options

    @commands.command(pass_context=True, description='Generates a sentence with a given voice then says it in the voice channel you are currently in')
    async def say(self, ctx: commands.Context, voice: str, *, sentence: str) -> None:
        """Say a sentence in the channel that the requestor is in.

        Args:
            ctx (commands.Context): Context of the request
            voice (str): Voice to use
            sentence (str): Sentence to say
        """
        author = ctx.message.author
        if author.voice is None:
            await ctx.send("You are not in a voice channel")
            return
        channel = author.voice.channel

        voice = voice.lower()

        log.info(
            "Trying to say %s with voice %s from %s in %s", sentence, voice, ctx.message.author, channel)

        try:
            error = self.verify_sentence(voice=voice, sentence=sentence)
            if error:
                await ctx.send(error)
                return
        except requests.exceptions.HTTPError as e:
            await ctx.send(f"Failed with {e.response.status_code}: {e.response.text}")
            return

        log.debug("Enqueuing %s", sentence)
        await self.bot.enqueue_sentence(
            sentence=sentence,
            voice=voice,
            guild=ctx.guild,
            channel=channel,
            respond=ctx.send,
        )
        log.debug("External done enqueuing")

    def get_channel_from_interaction(self, interaction: discord.Interaction) -> typing.Optional[discord.VoiceChannel]:
        """Extract a voice channel from an interaction

        Args:
            interaction (discord.Interaction): Slash command interaction

        Raises:
            Exception: Something unexpected happened
            UserNotInVoiceChannel: User tried to send a message but isn't in a voice channel

        Returns:
            discord.VoiceChannel: Voice channel from interaction
        """
        member = interaction.user
        if not isinstance(member, discord.Member):
            log.error("Found user instead of member")
            return None

        voice = member.voice
        if voice is None:
            log.error("User is not in a voice channel")
            return None

        channel = voice.channel
        if not isinstance(channel, discord.VoiceChannel):
            log.error("Incompatible channel: %s", channel)
            return None

        return channel

    @app_commands.command(name="say", description="Say a sentence using a chosen voice")
    @app_commands.autocomplete(voice=voice_autocomplete)
    @app_commands.autocomplete(sentence=sentence_autocomplete)
    async def say_slash(
        self,
        interaction: discord.Interaction,
        voice: str,
        sentence: str,
    ) -> None:
        """Similar to `say`, but for slash commands

        Args:
            interaction (discord.Interaction): Interaction for slash command
            voice (str): Voice to use
            sentence (str): Sentence to say
        """
        channel = self.get_channel_from_interaction(interaction=interaction)
        if channel is None:
            await interaction.response.send_message("You are not in a voice channel")
            return

        voice = voice.lower()

        log.info(
            "Trying to say %s with voice %s from %s in %s", sentence, voice, interaction.user, interaction.channel)

        try:
            error = self.verify_sentence(voice=voice, sentence=sentence)
            if error:
                await interaction.response.send_message(error)
                return
        except requests.exceptions.HTTPError as e:
            await interaction.response.send_message(f"Failed with {e.response.status_code}: {e.response.text}")
            return

        log.debug("Enqueuing %s", sentence)
        await self.bot.enqueue_sentence(
            sentence=sentence,
            voice=voice,
            guild=interaction.guild,
            channel=channel,
            respond=interaction.response.send_message,
            interaction=interaction,
        )
        log.debug("External done enqueuing")

    def get_random(self, voice: str, number_of_words: int) -> str:
        """Randomly generate a sentence for a given voice

        Args:
            voice (str): Voice to use
            number_of_words (int): Number of words in random sentence

        Returns:
            str: Random sentence
        """
        result = requests.get(
            f"{self.bot.api_url}/api/voice/{voice}/random", params={"num_words": number_of_words}, timeout=REQUEST_TIMEOUT_S)
        result.raise_for_status()

        r_content = json.loads(result.content)
        return r_content['sentence']

    @commands.command(pass_context=True, description='Generates a random sentence with a given voice and number of words')
    async def random(self, ctx: commands.Context, voice: str, num_words: int):
        """Say a random sentence

        Args:
            ctx (commands.Context): Context of request
            voice (str): Voice to use
            num_words (int): Number of words in random sentence
        """
        author = ctx.message.author
        if author.voice is None:
            await ctx.send("You are not in a voice channel")
            return
        channel = author.voice.channel

        log.info(
            "Trying to generate random sentence with %s words and voice %s from %s in %s", num_words, voice, ctx.message.author, channel)

        try:
            sentence = self.get_random(voice=voice, number_of_words=num_words)
        except requests.exceptions.HTTPError as e:
            await ctx.send(f"Failed with {e.response.status_code}: {e.response.text}")
            return

        await self.bot.enqueue_sentence(
            sentence=sentence,
            voice=voice,
            guild=ctx.guild,
            channel=channel,
            respond=ctx.send,
        )

    @app_commands.command(name="say_random", description="Generate a random sentence with a chosen voice")
    @app_commands.autocomplete(voice=voice_autocomplete)
    async def random_slash(
        self,
        interaction: discord.Interaction,
        voice: str,
        number_of_words: int,
    ) -> None:
        """Similar to `random`, but for slash commands

        Args:
            interaction (discord.Interaction): Interaction for slash command
            voice (str): Voice to use
            number_of_words (int): Number of words in random sentence
        """
        channel = self.get_channel_from_interaction(interaction=interaction)
        if channel is None:
            await interaction.response.send_message("You are not in a voice channel")
            return

        try:
            sentence = self.get_random(voice=voice, number_of_words=number_of_words)
        except requests.exceptions.HTTPError as e:
            await interaction.response.send_message(f"Failed with {e.response.status_code}: {e.response.text}")
            return

        await self.bot.enqueue_sentence(
            sentence=sentence,
            voice=voice,
            guild=interaction.guild,
            channel=channel,
            respond=interaction.response.send_message,
            interaction=interaction,
        )

    def get_random_generated(self, voice: str) -> str:
        """Get a random generated sentence for a given voice

        Args:
            voice (str): Voice to use

        Returns:
            str: Random sentence
        """
        result = requests.get(
            f"{self.bot.api_url}/api/voice/{voice}/random_generated", timeout=REQUEST_TIMEOUT_S)
        result.raise_for_status()
        
        r_content = json.loads(result.content)
        return r_content['sentence']

    @commands.command(name='randomgen', pass_context=True, description='Gets a random generated sentence from the given voice and number of words')
    async def random_generated(self, ctx: commands.Context, voice: str):
        """Say a random previously generated sentence

        Args:
            ctx (commands.Context): Context of request
            voice (str): Voice to use
        """
        author = ctx.message.author
        if author.voice is None:
            await ctx.send("You are not in a voice channel")
            return
        channel = author.voice.channel

        log.info(
            "Trying to get random generated sentence with voice %s from %s in %s", voice, ctx.message.author, channel)

        try:
            sentence = self.get_random_generated(voice=voice)
        except requests.exceptions.HTTPError as e:
            await ctx.send(f"Failed with {e.response.status_code}: {e.response.text}")
            return

        await self.bot.enqueue_sentence(
            sentence=sentence,
            voice=voice,
            guild=ctx.guild,
            channel=channel,
            respond=ctx.send,
        )

    @app_commands.command(name="say_random_gen", description="Say a random previously-generated sentence for a chosen voice")
    @app_commands.autocomplete(voice=voice_autocomplete)
    async def random_generated_slash(
        self,
        interaction: discord.Interaction,
        voice: str,
    ) -> None:
        """Similar to `random_generated`, but for slash commands

        Args:
            interaction (discord.Interaction): Interaction for slash command
            voice (str): Voice to use
        """
        channel = self.get_channel_from_interaction(interaction=interaction)
        if channel is None:
            await interaction.response.send_message("You are not in a voice channel")
            return

        try:
            sentence = self.get_random_generated(voice=voice)
        except requests.exceptions.HTTPError as e:
            await interaction.response.send_message(f"Failed with {e.response.status_code}: {e.response.text}")
            return

        await self.bot.enqueue_sentence(
            sentence=sentence,
            voice=voice,
            guild=interaction.guild,
            channel=channel,
            respond=interaction.response.send_message,
            interaction=interaction,
        )

    @commands.command(pass_context=True, description='Shows the available voices')
    async def voices(self, ctx: commands.Context):
        """Show available voices

        Args:
            ctx (commands.Context): Context of request
        """
        try:
            result = requests.get(
                f"{self.bot.api_url}/api/voices", timeout=REQUEST_TIMEOUT_S)
            result.raise_for_status()
        except requests.exceptions.HTTPError as e:
            await ctx.send(f"Failed with {e.response.status_code}: {e.response.text}")
            return

        r_content = json.loads(result.content)

        await ctx.send(f"Available voices: {r_content['voices']}")

    @commands.command(pass_context=True, description='Skip currently playing sentence')
    async def skip(self, ctx: commands.Context):
        """Skip currently playing sentence

        Args:
            ctx (commands.Context): Context of request
        """
        await self.bot.send_command(
            cmd=Command(
                ctx=ctx,
                command=CommandEnum.SKIP
            )
        )

    @app_commands.command(name="skip", description="Skip whatever is currently being said")
    async def skip_slash(
        self,
        interaction: discord.Interaction,
    ) -> None:
        """Similar to `skip`, but for slash commands

        Args:
            interaction (discord.Interaction): Interaction for slash command
        """
        await self.bot.send_command(
            cmd=Command(
                ctx=interaction,
                command=CommandEnum.SKIP
            )
        )


    @commands.command(pass_context=True, description='Disconnect current player')
    async def disconnect(self, ctx: commands.Context):
        """Disconnect active player

        Args:
            ctx (commands.Context): Context of request
        """
        await self.bot.send_command(
            cmd=Command(
                ctx=ctx,
                command=CommandEnum.DISCONNECT
            )
        )

    @app_commands.command(name="disconnect", description="Disconnect current player")
    async def disconnect_slash(
        self,
        interaction: discord.Interaction,
    ) -> None:
        """Similar to `disconnect`, but for slash commands

        Args:
            interaction (discord.Interaction): Interaction for slash command
        """
        await self.bot.send_command(
            cmd=Command(
                ctx=interaction,
                command=CommandEnum.DISCONNECT
            )
        )

    @commands.command(pass_context=True, description='(Same as disconnect)')
    async def stop(self, ctx: commands.Context):
        """Stop active player. Identical to `disconnect`.

        Args:
            ctx (commands.Context): Context of request
        """
        await self.bot.send_command(
            cmd=Command(
                ctx=ctx,
                command=CommandEnum.DISCONNECT
            )
        )

    @app_commands.command(name="stop", description="(Same as disconnect)")
    async def stop_slash(self, interaction: discord.Integration) -> None:
        """Similar to `stop`, but for slash commands

        Args:
            interaction (discord.Integration): Interaction for slash command
        """
        await self.bot.send_command(
            cmd=Command(
                ctx=interaction,
                command=CommandEnum.DISCONNECT
            )
        )


async def setup(bot: commands.Bot):
    """Extension setup function

    Args:
        bot (commands.Bot): Bot to add this cog to
    """
    await bot.add_cog(Say(bot))
