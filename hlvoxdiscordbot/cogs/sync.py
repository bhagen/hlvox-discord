import logging

from discord.ext import commands

log = logging.getLogger()


class Sync(commands.Cog):
    """Commands for synchronizing slash commands
    """

    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

    @commands.command(pass_context=True)
    async def sync(self, ctx: commands.context):
        """Synchronize slash commands

        Args:
            ctx (commands.context): Command context
        """
        ctx.bot.tree.clear_commands(guild=ctx.guild)
        synced = await ctx.bot.tree.sync(guild=ctx.guild)
        log.info("Synced %s", synced)
        await ctx.send(f"Synced {synced}")


async def setup(bot: commands.Bot):
    """Extension setup function

    Args:
        bot (commands.Bot): Bot to add this cog to
    """
    await bot.add_cog(Sync(bot))
