"""Vox discord bot
"""
import asyncio
import logging
import time
import urllib
from dataclasses import dataclass
from typing import Callable, Dict, Optional

import discord
from discord.ext import commands, tasks

from .cogs import say

log = logging.getLogger()

MAX_QUEUED_SENTENCES = 10

DISCONNECT_AFTER_LAST_AUDIO_PLAYED_S = 15

COGS = ["say", "sync"]


@dataclass
class Player:
    """Represents an active voice player in a channel
    """
    # TODO: asyncio queues are not thread safe. That might not matter given
    # how we are using them, but I am suspicious. Also, there are likely race conditions
    # with sending of say/commands and shutdowns, etc.
    say_queue: asyncio.Queue[say.SayCommand]
    command_queue: asyncio.Queue[say.Command]
    task: asyncio.Task


class HlVoxDiscordBot(commands.Bot):
    """Vox discord bot
    """

    def __init__(self, api_url: str, *args, **kwargs) -> None:
        """Initialize bot

        Args:
            api_url (str): API URL of vox voice server
        """
        intents = discord.Intents.none()
        intents.guilds = True
        intents.guild_messages = True
        intents.voice_states = True
        intents.message_content = True

        super().__init__(*args, **kwargs, intents=intents)

        discord.opus.load_opus("libopus.so.0")
        if not discord.opus.is_loaded():
            raise RuntimeError("Opus not loaded")

        self.api_url = api_url

        self.players: Dict[discord.Guild, Player] = {}
        self.players_lock = asyncio.Lock()

    async def setup_hook(self) -> None:
        """Start periodic tasks and load cogs/extensions
        """
        # pylint doesn't seem to believe that cleanup doesn't have a "start" member
        # pylint: disable=no-member
        self.cleanup.start()
        for cog in COGS:
            await self.load_extension("hlvoxdiscordbot.cogs." + cog)
            await self.tree.sync()

    @tasks.loop(minutes=1)
    async def cleanup(self) -> None:
        """Monitors active players and disconnects them
        after they have been idle for long enough
        """
        log.debug("Cleanup loop waiting for player lock")
        async with self.players_lock:
            log.debug("Cleanup loop got player lock")
            players = dict(self.players)
            for guild, player in players.items():
                if player.task.done():
                    if player.task.exception():
                        log.error("Task for guild %s encountered an exception: %s", guild, player.task.exception())
                    log.info("Cleaning up a player for %s", guild)
                    del self.players[guild]

    async def say_thread(self, command_queue: asyncio.Queue[say.Command], say_queue: asyncio.Queue[say.SayCommand], voice_client: discord.VoiceClient) -> None:
        """Say a sentence
        TODO: Update
        Args:
            command_queue (asyncio.Queue[say.Command]): Queue for commands
            say_queue (asyncio.Queue[say.SayCommand]): Queue for say commands
            voice_client (discord.VoiceClient): Voice client to use
        """
        disconnect_at = time.monotonic() + DISCONNECT_AFTER_LAST_AUDIO_PLAYED_S
        while say_queue.qsize() or voice_client.is_playing() or time.monotonic() < disconnect_at:
            if voice_client.is_playing():
                disconnect_at = time.monotonic() + DISCONNECT_AFTER_LAST_AUDIO_PLAYED_S

            cmd: Optional[say.Command] = None
            try:
                cmd = await asyncio.wait_for(command_queue.get(), timeout=1)
            except asyncio.TimeoutError:
                pass

            if cmd:
                # TODO: comparing with .value shouldn't be necessary, but it is?
                if cmd.command.value is say.CommandEnum.DISCONNECT.value:
                    await cmd.respond("Disconnecting")
                    voice_client.stop()
                    # TODO: race condition with someone sending a new command quickly after a disconnect
                    # TODO: clear queues?
                    break
                if cmd.command.value is say.CommandEnum.SKIP.value:
                    await cmd.respond("Skipping")
                    voice_client.stop()
                    # Wait a bit before letting the next phrase through
                    await asyncio.sleep(1)
                    continue

            say_cmd: Optional[say.SayCommand] = None
            if not voice_client.is_playing():
                try:
                    say_cmd = await asyncio.wait_for(say_queue.get(), timeout=1)
                except asyncio.TimeoutError:
                    pass

            if say_cmd:
                channel = say_cmd.channel
                if voice_client.channel != channel:
                    log.debug(
                        "Player is not in needed channel. Moving from %s to %s", voice_client.channel, channel)
                    await voice_client.move_to(channel)
                await say_cmd.edit(content=f"\u03BB {say_cmd.voice} \U0001F4E2 | {say_cmd.sentence.capitalize()}")

                params = {"sentence": say_cmd.sentence}
                audio_url = f"{self.api_url}/api/voice/{say_cmd.voice}/sentence_audio?{urllib.parse.urlencode(params)}"

                log.info("Playing from %s", audio_url)
                voice_client.play(discord.FFmpegPCMAudio(audio_url))
                disconnect_at = time.monotonic() + DISCONNECT_AFTER_LAST_AUDIO_PLAYED_S

        log.info("Disconnecting from %s", voice_client.channel)
        await voice_client.disconnect()

    async def enqueue_sentence(
        self,
        sentence: str,
        voice: str,
        guild: discord.Guild,
        channel: discord.VoiceChannel,
        respond: Callable,
        interaction: Optional[discord.Interaction] = None,
    ) -> None:
        """Enqueue a sentence to be played when possible

        Args:
            sentence (str): Sentence to say
            voice (str): Voice to use
            ctx (commands.Context): Context of request
        """
        log.debug("Waiting for lock before accessing players")
        async with self.players_lock:
            log.debug("Got players lock, looking for %s", guild)
            client = None
            for voice_client in self.voice_clients:
                if voice_client.guild == guild:
                    log.debug("Found existing voice client for %s", guild)
                    client = voice_client

            if not client:
                log.debug(
                    "No existing voice client found for %s, creating one", guild)
                client = await channel.connect(self_deaf=True)

            player = self.players.get(guild)
            if not player or player.task.done():
                log.debug(
                    "No existing player found (or it's task is done) for %s, creating one", guild)
                say_queue: asyncio.Queue[say.SayCommand] = asyncio.Queue(maxsize=10)
                command_queue: asyncio.Queue[say.Command] = asyncio.Queue(maxsize=10)
                task = asyncio.get_event_loop().create_task(self.say_thread(command_queue=command_queue, say_queue=say_queue, voice_client=client))
                player = Player(
                    say_queue=say_queue,
                    command_queue=command_queue,
                    task=task
                )
                self.players[guild] = player

        try:
            log.debug("Adding %s to queue", sentence)
            # I know this isn't guaranteed to work properly given raciness of adding to queue, but it'll work well enough
            message_text = f"\u03BB {voice} \U0001F551 | {sentence.capitalize()}"
            queue_size = 0
            if client.is_playing():
                queue_size = 1
            if player.say_queue.qsize() >= 1:
                queue_size += player.say_queue.qsize()

            if queue_size:
                message_text += (f"\n#{queue_size} in queue")
            message = await respond(message_text)
            edit = None
            if message:
                edit = message.edit
            elif interaction is not None:
                edit = interaction.edit_original_response
            cmd = say.SayCommand(
                sentence=sentence,
                voice=voice,
                guild=guild,
                channel=channel,
                edit=edit,
            )
            player.say_queue.put_nowait(cmd)

        except asyncio.QueueFull:
            await respond("Too many sentences queued!")
        log.debug("Done enqueuing sentence")

    async def send_command(self, cmd: say.Command):
        """Send a command to an active player

        Args:
            cmd (say.Command): Command to send
        """
        guild = cmd.ctx.guild
        player = self.players.get(guild)
        if not player or player.task.done():
            await cmd.respond("Nothing is currently playing!")
            return

        try:
            log.debug("Put command %s in queue for guild %s", cmd.command, guild)
            player.command_queue.put_nowait(cmd)
        except asyncio.QueueFull:
            await cmd.respond("Too many commands queued!")
