# HLVox Discord Bot
Simple dockerized bot that generates sentences from 
[HLVox-Website](https://gitlab.com/bhagen/hlvox-website) and plays them in a 
voice channel on Discord.

## Non-Docker Usage:
A Discord bot token is expected at `token/token.txt`. Consult Discord's
bot documentation for instructions on this.

The script needs the URL to your hlvox-website in order to generate voice
files. It is provided as a positional argument.

### Running the script:
Clone the repo
`git clone git@gitlab.com:bhagen/hlvox-discord.git`

Move into the directory
`cd hlvox-discord`

Install requirements using pipenv
`pipenv install`

Enter pipenv
`pipenv shell`

Run the script
`python run.py my.api.url /path/to/token.txt`


## Docker Usage

### Mounts:
Set the environment variable `API_URL` to your api url
Create a volume with a `token.txt` file in it

Start: `docker run -v token_volume:/token -e API_URL=https://my.api.url bhagen55/hlvoxdiscord`