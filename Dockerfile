FROM python:3.11.2-alpine3.16 as base

FROM base AS python-deps

RUN pip install pipenv
COPY Pipfile .
COPY Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

FROM base AS runtime

COPY --from=python-deps /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

RUN apk add --no-cache ffmpeg opus

ENV TOKEN_PATH /token/token.txt

COPY hlvoxdiscordbot /hlvoxdiscordbot
COPY run.py /

CMD python -u ./run.py